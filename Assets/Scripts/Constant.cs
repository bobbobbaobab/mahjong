﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant : MonoBehaviour {
	
	public static float mahjongGridWide = 1.54f;    //麻将网格的宽度
	public static float mahjongGridHeight = 2f;  //麻将网格的长度
	public static float mahjongGridHorizonalOffset = -0.18f;  //麻将每高一层，横向偏移的距离
	public static float mahjongGridVerticalOffset = 0.23f; //麻将每高一层，纵向偏移的距离
	public static bool isSFXon = true;
	public static bool isBGMon = true;


	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		
	}
}
