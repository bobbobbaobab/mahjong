﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class loading : MonoBehaviour {

	// Use this for initialization
	void Start () {

		Invoke("GoGameScene",1);
	
	}

	public void GoGameScene(){
		SceneManager.LoadScene ("game");
	}


	// Update is called once per frame
	void Update () {

		GameObject.Find ("Canvas/Text").GetComponent<Text> ().color = new Color (255,255,255,Mathf.Clamp(Time.time/0.8f,0f,1f));

	}
}
