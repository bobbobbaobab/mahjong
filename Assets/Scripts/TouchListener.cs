﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TouchListener : MonoBehaviour {





	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {




		if (Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Began) {
			
			Vector3 touchPositionInWorld3D = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
			Vector2 touchPositionInWorld2D = new Vector2 (touchPositionInWorld3D.x,touchPositionInWorld3D.y);
			Collider2D touchingCollider= Physics2D.OverlapPoint (touchPositionInWorld2D);
			if (touchingCollider) {


				iOSHapticFeedback.Instance.Trigger((iOSHapticFeedback.iOSFeedbackType)2);

				Destroy (touchingCollider.gameObject);



	
			}

		}

			
	}
}
