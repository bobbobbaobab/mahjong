﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mahjong : MonoBehaviour {
	
	public float mahjongSizeScale =0.67f;

	public int typeNumber;
	public float dotX;  
	public float dotY;
	public int layerNumber; //层号
	public bool isSeason = false; //是不是春夏秋冬
	public bool isFlower= false; //是不是梅兰竹菊
	public bool isBlocked= false; 
	public bool isCanceling= false; 
	public bool isShaking= false; 

	public bool canMeMove1 = false; //能否开始贝塞尔运动
	public bool canMeMove2 = false;//能否开始短暂逗留
	public bool canMeMove3 = false;//能否开始移出屏幕

	public bool canMeShake1 = false; 
	public bool canMeShake2 = false;
	public bool canMeShake3 = false;

	public bool canMeTurnRed1 = false; 
	public bool canMeTurnRed2 = false;

	public bool canMeShowHint1 = false; 
	public bool canMeShowHint2= false;


	public Vector2 shakeStartPosition; 
	public Vector2 shakeLeftPosition;
	public Vector2 shakeRightPosition;

	public float timeStart  ;  //记录某次运动开始的时刻
	public float timeStartTurnRed  ; //记录变红开始的时刻
	public float  timeStartShowHint;

	public bool willCreateParticle = false;  // 只有pair中左侧的麻将才生成粒子

	public Vector2 cancelStartPositon;
	public Vector2 cancelReadyPositon;
	public Vector2 cancelHappenPositon;
	public Vector2 cancelEndPositon;

	public int bottomLayerOrder; //最底层（shadow）的LayerOrder；

	// Use this for initialization
	void Start () {
		
	}


	//将麻将牌同步到坐标位置
	public void UpdatePositionInLayer (){
		
		float x = 0.6f+ Constant.mahjongGridWide *  dotX *mahjongSizeScale+ Constant.mahjongGridHorizonalOffset  * (layerNumber - 1)*mahjongSizeScale;
		float y =  Constant.mahjongGridHeight * dotY*mahjongSizeScale+Constant.mahjongGridVerticalOffset * (layerNumber - 1)*mahjongSizeScale ;
		transform.position = new Vector3 (x,y,-layerNumber);

	}

	public void ShowHint(){

		canMeShowHint2 = false;

		transform.Find ("white").GetComponent<SpriteRenderer> ().color =  new Color (255,255,255,0);

		timeStartShowHint = Time.time;

		canMeShowHint1 = true;
	
	}

	//初始化
	public void Initialize() {
		
		//将麻将牌放入MahjongGrid子物体下
		transform.parent = GameObject.Find ("MahjongGrid").GetComponent<Transform>();


		transform.localScale = new Vector3(mahjongSizeScale,mahjongSizeScale,1);

		//载入麻将牌body的图片
		transform.Find ("body").GetComponent<SpriteRenderer>().sprite = MahjongSprites.array[typeNumber];

		//判断是不是春夏秋冬
		if (typeNumber >= 35 && typeNumber <= 38) {
			isSeason = true;
		}
		//判断是不是梅兰竹菊
		if (typeNumber >= 39 && typeNumber <= 42) {
			isFlower = true;
		}

		//red的透明度设为零
		transform.Find ("red").GetComponent<SpriteRenderer> ().color =  new Color (255,255,255,0);
		//white的透明度设为零
		transform.Find ("white").GetComponent<SpriteRenderer> ().color =  new Color (255,255,255,0);

	}

	//sortingLayer确定
	public void SetSortingLayer(){

		SpriteRenderer[] srArray= transform.GetComponentsInChildren<SpriteRenderer> ();
		foreach(SpriteRenderer sr in srArray){ 
			sr.sortingLayerName = "MahjongLayer"+layerNumber;
		}

	}


	//设定每个sprite的LayerOrder （默认）
	public void SetLayerOrder(){

		bottomLayerOrder = (int) ((dotX-dotY) * 10);
		SetLayerOrder(bottomLayerOrder);
	
	}


	//设定每个sprite的LayerOrder （有参）
	public void SetLayerOrder(int bottomLayerOrder){
		
		transform.Find ("shadow").GetComponent<SpriteRenderer> ().sortingOrder = bottomLayerOrder;
		transform.Find ("body").GetComponent<SpriteRenderer> ().sortingOrder = bottomLayerOrder+1;
		transform.Find ("dark").GetComponent<SpriteRenderer> ().sortingOrder = bottomLayerOrder+2;
		transform.Find ("red").GetComponent<SpriteRenderer> ().sortingOrder = bottomLayerOrder+3;
		transform.Find ("white").GetComponent<SpriteRenderer> ().sortingOrder = bottomLayerOrder+4;
		transform.Find ("choose").GetComponent<SpriteRenderer> ().sortingOrder = 1000;
	}



	public void LiftSortingLayer(){
		
		SpriteRenderer[] srArray= transform.GetComponentsInChildren<SpriteRenderer> ();
		foreach(SpriteRenderer sr in srArray){ 
			sr.sortingLayerName = "MahjongLayerTop";
		}
	
	}


	public void DoLeftMove(float centerX , float centerY){

		willCreateParticle = true;

		cancelStartPositon = transform.position;
		cancelReadyPositon = new Vector2 (transform.position.x-Constant.mahjongGridWide*mahjongSizeScale,centerY);
		cancelHappenPositon = new Vector2 (centerX-Constant.mahjongGridWide/2*mahjongSizeScale,centerY);
		cancelEndPositon =  new Vector2 (centerX-Constant.mahjongGridWide/2*mahjongSizeScale,centerY-25);

		timeStart = Time.time;
		canMeMove1 = true;
	}

	public void DoRightMove(float centerX , float centerY){
		
		cancelStartPositon = transform.position;
		cancelReadyPositon = new Vector2 (transform.position.x+Constant.mahjongGridWide*mahjongSizeScale,centerY);
		cancelHappenPositon = new Vector2 (centerX+Constant.mahjongGridWide/2*mahjongSizeScale,centerY);
		cancelEndPositon =  new Vector2 (centerX+Constant.mahjongGridWide/2*mahjongSizeScale,centerY-25);

		timeStart = Time.time;
		canMeMove1 = true;
	}

	public void Shake() {



			isShaking = true;
			timeStart = Time.time;
			shakeStartPosition = transform.position;
			shakeLeftPosition = new Vector2 (transform.position.x - 0.1f , transform.position.y);
			shakeRightPosition = new Vector2 (transform.position.x + 0.1f , transform.position.y);

			canMeShake1 = true;

		
	}

	public void TurnRed() {



		canMeTurnRed2 = false;

		transform.Find ("red").GetComponent<SpriteRenderer> ().color =  new Color (255,255,255,0);

		timeStartTurnRed = Time.time;

		canMeTurnRed1 = true;

	}

	// Update is called once per frame
	void Update () {

		if (canMeShowHint1 == true) {

			transform.Find ("white").GetComponent<SpriteRenderer> ().color =  new Color (255,255,255,Mathf.Clamp(  (Time.time - timeStartShowHint)/0.25f ,0f,1f  ));


			if (Time.time >= timeStartShowHint + 0.25f) {

				timeStartShowHint = Time.time;
				canMeShowHint1= false;
				canMeShowHint2 = true;
			}	
		}

		if (canMeShowHint2 == true) {

			transform.Find ("white").GetComponent<SpriteRenderer> ().color =  new Color (255,255,255,1-Mathf.Clamp(  (Time.time - timeStartShowHint)/0.25f ,0f,1f  ));

			if (Time.time >= timeStartShowHint + 0.3f) {

				canMeShowHint2 = false;
			}	
		}



		if (canMeTurnRed1 == true) {

			transform.Find ("red").GetComponent<SpriteRenderer> ().color =  new Color (255,255,255,Mathf.Clamp(  (Time.time - timeStartTurnRed)/0.5f ,0f,1f  ));


			if (Time.time >= timeStartTurnRed + 0.5f) {

				timeStartTurnRed = Time.time;
				canMeTurnRed1= false;
				canMeTurnRed2 = true;
			}	
		}

		if (canMeTurnRed2 == true) {

			transform.Find ("red").GetComponent<SpriteRenderer> ().color =  new Color (255,255,255,1-Mathf.Clamp(  (Time.time - timeStartTurnRed)/0.5f ,0f,1f  ));


			if (Time.time > timeStartTurnRed + 0.6f) {


				canMeTurnRed2 = false;
	
			
			}	
		
		}




		if (canMeShake1 == true) {

			transform.position = Vector2.Lerp (shakeStartPosition, shakeLeftPosition, (Time.time - timeStart) / 0.1f);
			if (Time.time >= timeStart + 0.1f) {

				timeStart = Time.time;
				canMeShake1= false;
				canMeShake2 = true;
			}	
		}

		if (canMeShake2 == true) {

			transform.position = Vector2.Lerp (shakeLeftPosition, shakeRightPosition, (Time.time - timeStart) / 0.1f);
			if (Time.time >= timeStart + 0.1f) {

				timeStart = Time.time;
				canMeShake2= false;
				canMeShake3 = true;
			}	
		}

		if (canMeShake3 == true) {

			transform.position = Vector2.Lerp (shakeRightPosition, shakeStartPosition, (Time.time - timeStart) / 0.1f);

			if (Time.time > timeStart + 0.1f) {

				canMeShake3 = false;
				isShaking = false;
				UpdatePositionInLayer ();
			
			}	
		
		}



		if (canMeMove1 == true) {

			float t =  Mathf.Clamp(Time.time-timeStart,0f,0.5f) /0.5f;

			float x = (1-t)*(1-t)*cancelStartPositon .x+2*t*(1-t)*cancelReadyPositon .x+t*t*cancelHappenPositon.x;
			float y = (1-t)*(1-t)*cancelStartPositon .y+2*t*(1-t)*cancelReadyPositon .y+t*t*cancelHappenPositon.y;

			transform.position = new Vector2 (x, y);

			if (Time.time >= timeStart + 0.5f) {
				
				if (willCreateParticle == true) {
					ParticleMaker.Instance.createParticleImpact (cancelHappenPositon);
					ParticleMaker.Instance.createParticleImpact2 (cancelHappenPositon);
				}

				SoundMaker.Instance.createSoundImpact();

				iOSHapticFeedback.Instance.Trigger((iOSHapticFeedback.iOSFeedbackType)2);

				timeStart = Time.time;
				canMeMove1 = false;
				canMeMove2 = true;

			}
		
		
		}

	
		if (canMeMove2 == true) {

			if (Time.time >= timeStart + 0.2f) {

				timeStart = Time.time;
				canMeMove2 = false;
				canMeMove3 = true;
			}


		}

		if (canMeMove3 == true) {
			
			transform.position = Vector2.Lerp (cancelHappenPositon, cancelEndPositon, (Time.time - timeStart) / 1);
			
		}
			
	
	}










}
