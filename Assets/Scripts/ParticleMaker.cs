﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleMaker : MonoBehaviour {

	public static ParticleMaker Instance;

	public GameObject particleExplosion;
	public GameObject particleImpact;
	public GameObject particleShuffle;
	public GameObject particleHint;
	public GameObject particleWholeScreen;
	public GameObject particleImpact2;

	void Awake()
	{
		Instance = this;
	}

	public void createParticleImpact(Vector2 position)
	{
		GameObject go = Instantiate (particleImpact);
		go.GetComponent<Transform> ().position = position;
		Destroy (go,2f);
	}

	public void createParticleImpact2(Vector2 position)
	{
		GameObject go = Instantiate (particleImpact2);
		go.GetComponent<Transform> ().position = position;
		Destroy (go,2f);
	}


	public void createParticleWholeScreen()
	{
		GameObject go = Instantiate (particleWholeScreen);
		Destroy (go,2f);
	}

	public void createParticleShuffle()
	{
		GameObject go = Instantiate (particleShuffle);
		Destroy (go,2f);
	}

	public void createParticleHint()
	{
		GameObject go = Instantiate (particleHint);
		Destroy (go,2f);
	}

	public void createParticleExplosion()
	{
		GameObject go = Instantiate (particleExplosion);
		float x = Random.Range (-3f,3f);
		float y = Random.Range (-2.5f,6f);
		go.GetComponent<Transform> ().position = new Vector2(x,y);
		Destroy (go,2f);
	}

	void Start () 
	{
		
	}
}
