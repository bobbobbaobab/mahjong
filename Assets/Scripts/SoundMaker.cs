﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMaker : MonoBehaviour {

	public static SoundMaker Instance;


	public AudioClip shuffle;
	public AudioClip impact;
	public AudioClip choose;
	public AudioClip cantChoose;
	public AudioClip pairSuccess;
	public AudioClip newGame;
	public AudioClip explosion;
	public AudioClip hint;
	public AudioClip menu;

	void Awake()
	{

		Instance = this;
	}

	private void MakeSound(AudioClip originalClip)
	{
		// As it is not 3D audio clip, position doesn't matter.
		AudioSource.PlayClipAtPoint(originalClip, transform.position);
	}

	public void createSoundHint()
	{
		if(Constant.isSFXon)
		MakeSound(hint);
	
	}

	public void createSoundExplosion()
	{
		if(Constant.isSFXon)
		MakeSound(explosion);
	}


	public void createSoundShuffle()
	{
		if(Constant.isSFXon)
		MakeSound(shuffle);
	}

	public void createSoundNewGame()
	{
		if(Constant.isSFXon)
		MakeSound(newGame);
	}

	public void createSoundImpact()
	{
		if(Constant.isSFXon)
		MakeSound(impact);
	}

	public void createSoundChoose()
	{
		if(Constant.isSFXon)
		MakeSound(choose);
	}

	public void createSoundCantChoose()
	{
		if(Constant.isSFXon)
		MakeSound(cantChoose);
	}

	public void createSoundPairSuccess()
	{
		if(Constant.isSFXon)
		MakeSound(pairSuccess);
	}


	public void createSoundMenu()
	{
		if(Constant.isSFXon)
			MakeSound(menu);
	}

	public void switchSFX(){
		
		if (Constant.isSFXon)
			Constant.isSFXon = false;
		
		else
			Constant.isSFXon = true;
	
	
	}

	public void switchBGM(){


		if (GameObject.Find ("BGM").GetComponent<AudioSource> ().mute == false)
			
			GameObject.Find ("BGM").GetComponent<AudioSource> ().mute = true;

		else
			GameObject.Find ("BGM").GetComponent<AudioSource> ().mute = false;


	}




}
