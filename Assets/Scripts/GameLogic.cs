﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;



public class GameLogic : MonoBehaviour {

	public static GameLogic Instance;

	void Awake()
	{
		Instance = this;
		buttonCoolDownTime = Time.time;
	}
		
	public GameObject mahjongGO; //为了拖拽麻将的prefab
	private  List<GameObject> pairMahjongList=new  List<GameObject> (); //用于暂时储存等待配对的麻将；
	private List<GameObject> livingMahjongList = new List<GameObject> (); //用于储存地图上仍存在的麻将
	public float buttonCoolDownTime;


	public void NewGame(){


		SoundMaker.Instance.createSoundNewGame ();
		ParticleMaker.Instance.createParticleWholeScreen ();

		foreach (GameObject go in livingMahjongList) {
			Destroy (go);
		}
		pairMahjongList.Clear ();
		livingMahjongList.Clear ();

		//指定一张地图号
		//int mapNumber = 100;
			
		//随机取一张地图号
		int mapNumber = UnityEngine.Random.Range (1,MapStorer.mapAmount+1);

		if (mapNumber <= 9) {
			GameObject.Find ("Canvas/textMapCode").GetComponent<Text> ().text = "map#00" + mapNumber;
		} else if (mapNumber >= 10 && mapNumber <= 99) {
			GameObject.Find ("Canvas/textMapCode").GetComponent<Text> ().text = "map#0" + mapNumber;
		} else if (mapNumber >= 100 && mapNumber <= 999) {
			GameObject.Find ("Canvas/textMapCode").GetComponent<Text> ().text = "map#" + mapNumber;
		}

		List<float[]> gridDotList = MapStorer.Instance.GetGridDotList(mapNumber);

		int gridDotNumber = gridDotList.Count; //坐标点总数（即麻将牌总数）
		List <int> TypeNumberSequence = new List <int>();  //创建牌类型序列

		//先在序列中装入花牌类型

			TypeNumberSequence .Add(35);
			TypeNumberSequence .Add(36);
			TypeNumberSequence .Add(37);
			TypeNumberSequence .Add(38);
			TypeNumberSequence .Add(39);
			TypeNumberSequence .Add(40);
			TypeNumberSequence .Add(41);
			TypeNumberSequence .Add(42);


		List<int> availableTypeNumberList = new List<int> (); //创建可选的麻将类型数字的容器

		//初始化容器
		for(int i=1;i<=34;i++){
			availableTypeNumberList.Add (i);
		}


		//填满牌类型序列

		for(int i=8;i<gridDotNumber;i=i+2){

			//随机抽一个数
			int gottenTypeNumber= availableTypeNumberList [UnityEngine.Random.Range (0, availableTypeNumberList.Count)];

			//连续装2个
			for(int j =0;j<2;j++){
				TypeNumberSequence.Add(gottenTypeNumber);
			}

			//从该容器中删除该数字
			availableTypeNumberList.Remove(gottenTypeNumber);

			//当牌类型用完时重新初始化
			if(availableTypeNumberList.Count==0){
				for(int j=1;j<=34;j++){
					availableTypeNumberList.Add (j);
				}
			}
		
		}

		gridDotList =  gridDotList.OrderBy(c => Guid.NewGuid()).ToList<float[]>();

		for (int i = 0; i <gridDotNumber ; i++) {
			
			CreateMahjong (gridDotList[i],TypeNumberSequence[i]);
		
		}

		Shuffle ();
	
	}



	public void ButtonNewGame (){
		
		if (Time.time - buttonCoolDownTime > 0.5f) {

			buttonCoolDownTime = Time.time;
			NewGame ();
	
		}
	}

	// Use this for initialization
	void Start () {
		
		NewGame ();


	}



	//该位置是否存在不处于消除中的麻将
	public bool DoesMahjongExist(int layer, float x, float y){

		GameObject go = livingMahjongList.Find (t=>t.GetComponent<Mahjong>().dotX == x && t.GetComponent<Mahjong>().dotY == y&& t.GetComponent<Mahjong>().layerNumber == layer);
		if (go == null) {
			return false;
		} else {

			if (go.GetComponent<Mahjong> ().isCanceling == true) {
				return false;
			} else {
				return true;	
			}
		}
		
	}





	public void CreateMahjong(float[] dot, int type){
	
	
		//实例化一个麻将牌
		GameObject go = Instantiate (mahjongGO);

		//麻将牌各属性赋值
		go.GetComponent<Mahjong>().typeNumber =type;
		go.GetComponent<Mahjong> ().dotX = dot[1];
		go.GetComponent<Mahjong> ().dotY = dot[2];
		go.GetComponent<Mahjong> ().layerNumber = (int)dot[0];

		//调用麻将的初始化方法
		go.GetComponent<Mahjong>().Initialize();

		//装入存活容器
		livingMahjongList.Add(go);

	
	}


	public void SwitchTutorial(){
		SoundMaker.Instance.createSoundMenu ();
		if (GameObject.Find ("Canvas/Tutorial").GetComponent<Image> ().enabled == true)
			GameObject.Find ("Canvas/Tutorial").GetComponent<Image> ().enabled = false;
		else
			GameObject.Find ("Canvas/Tutorial").GetComponent<Image> ().enabled = true;
	}

	public GameObject getMahjongByDot(int layer, float x, float y){

		return  livingMahjongList.Find (t=>t.GetComponent<Mahjong>().dotX == x && t.GetComponent<Mahjong>().dotY == y&& t.GetComponent<Mahjong>().layerNumber == layer);

	}

	public void ButtonShuffle(){

		if (Time.time - buttonCoolDownTime > 0.5f) {
			buttonCoolDownTime = Time.time;
			Shuffle ();
			ParticleMaker.Instance.createParticleShuffle ();
			SoundMaker.Instance.createSoundShuffle ();
		}
	
	}





	public void Shuffle(){

		bool isDead = false;


		List<GameObject> tempMahjongList = new List<GameObject> ();


		while (livingMahjongList.Count > 0) {

			//按层数的倒序对livingMahjongList排序
			livingMahjongList = livingMahjongList.OrderByDescending (c => c.GetComponent<Mahjong>().layerNumber).ToList<GameObject>();

			List<GameObject> notBlockedMahjongList =  new List<GameObject> ();

			foreach( GameObject go in livingMahjongList) {
				CheckBlock (go);
				if (go.GetComponent<Mahjong> ().isBlocked == false)
					notBlockedMahjongList.Add (go);
			}
		
			if (notBlockedMahjongList.Count < 2) { //死局
			
				 isDead = true;

				foreach( GameObject go in livingMahjongList) {
					tempMahjongList.Add (go);
				}

				livingMahjongList.Clear ();


			} else {
				GameObject go1 = notBlockedMahjongList [0];
				GameObject go2 = notBlockedMahjongList [1];		
				GameObject go3 = livingMahjongList [livingMahjongList.Count-1];
				GameObject go4 = livingMahjongList.Find (t => CanBeCanceled (go3, t));

				float tempDotX, tempDotY;
				int tempLayer;

				tempDotX = go1.GetComponent<Mahjong> ().dotX;
				go1.GetComponent<Mahjong> ().dotX = go3.GetComponent<Mahjong> ().dotX;
				go3.GetComponent<Mahjong> ().dotX = tempDotX;

				tempDotY = go1.GetComponent<Mahjong> ().dotY;
				go1.GetComponent<Mahjong> ().dotY = go3.GetComponent<Mahjong> ().dotY;
				go3.GetComponent<Mahjong> ().dotY = tempDotY;
						
				tempLayer = go1.GetComponent<Mahjong> ().layerNumber;
				go1.GetComponent<Mahjong> ().layerNumber = go3.GetComponent<Mahjong> ().layerNumber;
				go3.GetComponent<Mahjong> ().layerNumber = tempLayer;
			
				tempDotX = go2.GetComponent<Mahjong> ().dotX;
				go2.GetComponent<Mahjong> ().dotX = go4.GetComponent<Mahjong> ().dotX;
				go4.GetComponent<Mahjong> ().dotX = tempDotX;

				tempDotY = go2.GetComponent<Mahjong> ().dotY;
				go2.GetComponent<Mahjong> ().dotY = go4.GetComponent<Mahjong> ().dotY;
				go4.GetComponent<Mahjong> ().dotY = tempDotY;

				tempLayer = go2.GetComponent<Mahjong> ().layerNumber;
				go2.GetComponent<Mahjong> ().layerNumber = go4.GetComponent<Mahjong> ().layerNumber;
				go4.GetComponent<Mahjong> ().layerNumber = tempLayer;

				livingMahjongList.Remove (go3);  livingMahjongList.Remove (go4);
				tempMahjongList.Add (go3);tempMahjongList.Add (go4);			
			}		
		}


		livingMahjongList = tempMahjongList;


		//重新判断每个麻将的dark与层中的前后
		foreach( GameObject go in tempMahjongList) {
			
			CheckBlock (go);
			go.GetComponent<Mahjong> ().UpdatePositionInLayer();
			go.GetComponent<Mahjong> ().SetSortingLayer ();
			go.GetComponent<Mahjong> ().SetLayerOrder ();
			go.transform.Find ("choose").GetComponent<SpriteRenderer> ().enabled = false;	
		}

		if (isDead){
		GameObject.Find ("Canvas/textAvailableMatches").GetComponent<Text>().text = "GAME OVER";
		GameObject.Find ("Canvas/textAvailableMatches").GetComponent<Text> ().color = new Color (255, 0, 0);
		}else
			UpdateAvailableMatches ();
		

		pairMahjongList.Clear ();

	}
	//消除一对麻将
	public void CancelPair(GameObject go1,GameObject go2){
	


		go1.GetComponent<BoxCollider2D> ().enabled = false;
		go2.GetComponent<BoxCollider2D> ().enabled = false;
		go1.GetComponent<Mahjong> ().isCanceling = true;
		go2.GetComponent<Mahjong> ().isCanceling = true;

		go1.GetComponent<Mahjong> ().LiftSortingLayer ();
		go2.GetComponent<Mahjong> ().LiftSortingLayer ();

		//设置右边的麻将层高于左边
		go1.GetComponent<Mahjong> ().SetLayerOrder (1);
		go2.GetComponent<Mahjong> ().SetLayerOrder (10);

		float centerX = (go1.transform.position.x + go2.transform.position.x) / 2;
		float centerY = (go1.transform.position.y + go2.transform.position.y) / 2;

		go1.GetComponent<Mahjong> ().DoLeftMove (centerX,centerY);
		go2.GetComponent<Mahjong> ().DoRightMove (centerX,centerY);


		CheckBlockAfterCancel(go1);
		CheckBlockAfterCancel(go2);

		livingMahjongList.Remove (go1);
		livingMahjongList.Remove (go2);

		Destroy (go1, 3f);
		Destroy (go2, 3f);

	}


	//检查某麻将是否被阻挡，并开/关dark
	public void CheckBlock (GameObject go){

		float x =go.GetComponent<Mahjong> ().dotX;
		float y = go.GetComponent<Mahjong> ().dotY;
		int layer= go.GetComponent<Mahjong> ().layerNumber;
		
		bool rightFlag = false;
		bool leftFlag = false;
		bool aboveFlag = false;

		rightFlag = rightFlag || DoesMahjongExist (layer,x+1,y);
		rightFlag = rightFlag || DoesMahjongExist (layer,x+1,y+0.5f);
		rightFlag = rightFlag || DoesMahjongExist (layer,x+1,y-0.5f);

		leftFlag = leftFlag || DoesMahjongExist (layer,x-1,y);
		leftFlag = leftFlag || DoesMahjongExist (layer,x-1,y+0.5f);
		leftFlag = leftFlag || DoesMahjongExist (layer,x-1,y-0.5f);

		aboveFlag = aboveFlag || DoesMahjongExist (layer+1,x,y);
		aboveFlag = aboveFlag || DoesMahjongExist (layer+1,x+0.5f,y);
		aboveFlag = aboveFlag || DoesMahjongExist (layer+1,x-0.5f,y);
		aboveFlag = aboveFlag || DoesMahjongExist (layer+1,x,y+0.5f);
		aboveFlag = aboveFlag || DoesMahjongExist (layer+1,x,y-0.5f);
		aboveFlag = aboveFlag || DoesMahjongExist (layer+1,x+0.5f,y+0.5f);	
		aboveFlag = aboveFlag || DoesMahjongExist (layer+1,x+0.5f,y-0.5f);
		aboveFlag = aboveFlag || DoesMahjongExist (layer+1,x-0.5f,y+0.5f);
		aboveFlag = aboveFlag || DoesMahjongExist (layer+1,x-0.5f,y-0.5f);


		if ( !(!rightFlag || !leftFlag) || aboveFlag) {

			go.GetComponent<Mahjong> ().isBlocked = true;
			go.transform.Find ("dark").GetComponent<SpriteRenderer> ().enabled = true;

		} else {
			go.GetComponent<Mahjong> ().isBlocked = false;
			go.transform.Find ("dark").GetComponent<SpriteRenderer> ().enabled = false;
		}
	
	}


	//某个坐标上如果有麻将，则CheckBlock它
	public void CheckBlockByDot(int layer, float x , float y){

		if (DoesMahjongExist (layer, x, y) == true) {
	
			CheckBlock (getMahjongByDot (layer, x, y));
		}
	
	}

	//某个坐标上如果有麻将，则它执行TurnRed
	public void TurnRedByDot(int layer, float x , float y){

		if (DoesMahjongExist (layer, x, y) == true) {

			getMahjongByDot (layer, x, y).GetComponent<Mahjong> ().TurnRed ();
		
		}

	}

	//更新AvailableMatches文本数字，也能返回该数字
	public int UpdateAvailableMatches( ){
			
		List<int> checkList = new List<int>(); //存放所有没被阻挡的麻将的类型号

		foreach(GameObject  go in livingMahjongList){
			
			if (go.GetComponent<Mahjong> ().isBlocked == false) {
			
				if (go.GetComponent<Mahjong> ().isFlower) {
					checkList.Add (39);  //统一花牌序号
				} else if (go.GetComponent<Mahjong> ().isSeason) {
					checkList.Add (35);  //统一花牌序号
				}else{
					checkList.Add (go.GetComponent<Mahjong> ().typeNumber); 
				}		
			}
		
		}

		int result = 2*checkList.GroupBy (t => t).Where (t => t.Count () == 4).Count ();
		result += checkList.GroupBy (t => t).Where (t => t.Count () == 3|| t.Count () == 2).Count ();
			
		GameObject.Find ("Canvas/textAvailableMatches").GetComponent<Text>().text = "Available Matches: "+result;

		if (result == 0 && livingMahjongList.Count != 0) {
			GameObject.Find ("Canvas/textAvailableMatches").GetComponent<Text> ().color = new Color (1f,90/255f,0f);
		} else if (result == 0 && livingMahjongList.Count == 0) {
			GameObject.Find ("Canvas/textAvailableMatches").GetComponent<Text>().text = "Congratulations !";
			GameObject.Find ("Canvas/textAvailableMatches").GetComponent<Text> ().color = new Color (0, 255, 0);
		} else {
			GameObject.Find ("Canvas/textAvailableMatches").GetComponent<Text> ().color = new Color (255, 255, 255);
		}

		return result;
	}



	//消除的麻将周围的麻将可能被解除Block时，执行该方法
	public void CheckBlockAfterCancel (GameObject go){
		
		float x =go.GetComponent<Mahjong> ().dotX;
		float y = go.GetComponent<Mahjong> ().dotY;
		int layer= go.GetComponent<Mahjong> ().layerNumber;
	
		CheckBlockByDot (layer,x+1,y);
		CheckBlockByDot (layer,x+1,y+0.5f);
		CheckBlockByDot (layer,x+1,y-0.5f);

		CheckBlockByDot (layer,x-1,y);
		CheckBlockByDot (layer,x-1,y+0.5f);
		CheckBlockByDot (layer,x-1,y-0.5f);


		CheckBlockByDot (layer-1,x,y);
		CheckBlockByDot (layer-1,x+0.5f,y);
		CheckBlockByDot (layer-1,x-0.5f,y);
		CheckBlockByDot (layer-1,x,y+0.5f);
		CheckBlockByDot (layer-1,x,y-0.5f);
		CheckBlockByDot (layer-1,x+0.5f,y+0.5f);	
		CheckBlockByDot (layer-1,x+0.5f,y-0.5f);
		CheckBlockByDot (layer-1,x-0.5f,y+0.5f);
		CheckBlockByDot (layer-1,x-0.5f,y-0.5f);
	
	}

	//将周围阻挡的麻将变红
	public void TurnRedBlockMahjongs (GameObject go){
	
		float x =go.GetComponent<Mahjong> ().dotX;
		float y = go.GetComponent<Mahjong> ().dotY;
		int layer= go.GetComponent<Mahjong> ().layerNumber;

		TurnRedByDot (layer,x+1,y);
		TurnRedByDot (layer,x+1,y+0.5f);
		TurnRedByDot (layer,x+1,y-0.5f);

		TurnRedByDot (layer,x-1,y);
		TurnRedByDot (layer,x-1,y+0.5f);
		TurnRedByDot (layer,x-1,y-0.5f);


		TurnRedByDot (layer+1,x,y);
		TurnRedByDot (layer+1,x+0.5f,y);
		TurnRedByDot (layer+1,x-0.5f,y);
		TurnRedByDot (layer+1,x,y+0.5f);
		TurnRedByDot (layer+1,x,y-0.5f);
		TurnRedByDot (layer+1,x+0.5f,y+0.5f);	
		TurnRedByDot (layer+1,x+0.5f,y-0.5f);
		TurnRedByDot (layer+1,x-0.5f,y+0.5f);
		TurnRedByDot (layer+1,x-0.5f,y-0.5f);
	
	
	}

	//判断两个麻将是否能消除
	public bool CanBeCanceled (GameObject go1,GameObject go2 ){



		bool flag1 = go1.GetComponent<Mahjong> ().isSeason && go2.GetComponent<Mahjong> ().isSeason;
		bool flag2 = go1.GetComponent<Mahjong> ().isFlower && go2.GetComponent<Mahjong> ().isFlower;
		bool flag3 = go1.GetComponent<Mahjong> ().typeNumber == go2.GetComponent<Mahjong> ().typeNumber;



		return flag1 || flag2 || flag3;
	
	}

	public void Hint(){

		SoundMaker.Instance.createSoundHint ();
		ParticleMaker.Instance.createParticleHint ();
	
		livingMahjongList = livingMahjongList.OrderBy (c => Guid.NewGuid ()).ToList<GameObject>();

		foreach (GameObject  go1 in livingMahjongList) {

			if (go1.GetComponent<Mahjong> ().isBlocked == false) {
	
				GameObject go2 = livingMahjongList.FindLast (t => t.GetComponent<Mahjong> ().isBlocked == false && CanBeCanceled (go1, t));

				if (go1 != go2) {
					
					go1.GetComponent<Mahjong> ().ShowHint ();
					go2.GetComponent<Mahjong> ().ShowHint ();
					return;
				}
			
			}
		}
	}

	public void StartGameClear(){
		
		GameObject.Find ("Canvas/buttonHint").GetComponent<Button> ().enabled = false;
		GameObject.Find ("Canvas/buttonShuffle").GetComponent<Button> ().enabled = false;
		GameObject.Find ("Canvas/buttonNewGame").GetComponent<Button> ().enabled = false;
	
		Invoke ("MakeExplosionEffect",1.2f);
		Invoke ("MakeExplosionEffect",1.9f);
		Invoke ("MakeExplosionEffect",2.6f);
		Invoke ("MakeExplosionEffect",3.3f);
		Invoke ("EndGameClear",4.0f);
	}

	public void EndGameClear(){
		GameObject.Find ("Canvas/buttonHint").GetComponent<Button> ().enabled = enabled;
		GameObject.Find ("Canvas/buttonShuffle").GetComponent<Button> ().enabled = enabled;
		GameObject.Find ("Canvas/buttonNewGame").GetComponent<Button> ().enabled = enabled;
		NewGame ();
	}


	public void MakeExplosionEffect(){
		ParticleMaker.Instance.createParticleExplosion ();
		SoundMaker.Instance.createSoundExplosion ();
		iOSHapticFeedback.Instance.Trigger ((iOSHapticFeedback.iOSFeedbackType)3);
	}


	// Update is called once per frame
	void Update ()
	{

		//测试用
		if (Input.GetKeyDown (KeyCode.Space)) {
			ScreenCapture.CaptureScreenshot ("screenshot.png");
		}
		if (Input.GetKeyDown (KeyCode.A)) {
			NewGame ();
		}



		if ( Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Began    ) {
			Vector3  touchPositionInWorld3D = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
		  Vector2 touchPositionInWorld2D = new Vector2 (touchPositionInWorld3D.x, touchPositionInWorld3D.y);
			Collider2D touchingCollider = Physics2D.OverlapPoint (touchPositionInWorld2D);

			if (touchingCollider != null) {


				if (touchingCollider.GetComponent<Mahjong> ().isBlocked == false) {


					if (pairMahjongList.Count == 0) {

						//亮黄色

						touchingCollider.transform.Find ("choose").GetComponent<SpriteRenderer> ().enabled = true;
					

						SoundMaker.Instance.createSoundChoose ();
						iOSHapticFeedback.Instance.Trigger ((iOSHapticFeedback.iOSFeedbackType)0);

						//加入配对容器
						pairMahjongList.Add (touchingCollider.gameObject);
					
					} else if (pairMahjongList.Count == 1) {

						//如果点的是自己
						if (touchingCollider.gameObject == pairMahjongList [0]) {  
							
							//灭黄色
							touchingCollider.transform.Find ("choose").GetComponent<SpriteRenderer> ().enabled = false;	

							SoundMaker.Instance.createSoundChoose ();
							iOSHapticFeedback.Instance.Trigger ((iOSHapticFeedback.iOSFeedbackType)0);


							//清除配对容器
							pairMahjongList.Remove (touchingCollider.gameObject);
						} 

						//如果不能消除
						else if (!CanBeCanceled (touchingCollider.gameObject, pairMahjongList [0])) {

							//换黄色
							pairMahjongList [0].transform.Find ("choose").GetComponent<SpriteRenderer> ().enabled = false;	
							touchingCollider.transform.Find ("choose").GetComponent<SpriteRenderer> ().enabled = true;
							SoundMaker.Instance.createSoundChoose ();
							iOSHapticFeedback.Instance.Trigger ((iOSHapticFeedback.iOSFeedbackType)0);

							//配对容器出进
							pairMahjongList.Remove (pairMahjongList [0]);
							pairMahjongList.Add (touchingCollider.gameObject);

						} else { //可以消除
							
							//灭黄色
							pairMahjongList [0].transform.Find ("choose").GetComponent<SpriteRenderer> ().enabled = false;
							iOSHapticFeedback.Instance.Trigger((iOSHapticFeedback.iOSFeedbackType)0);
							SoundMaker.Instance.createSoundPairSuccess ();

							//执行消除
							if (pairMahjongList [0].transform.position.x <= touchingCollider.transform.position.x) {		
								CancelPair (pairMahjongList [0], touchingCollider.gameObject);										
							} else {
								CancelPair (touchingCollider.gameObject, pairMahjongList [0]);
							}

							//更新Available Matches文本数字
							UpdateAvailableMatches ();

							//清空配对容器
							pairMahjongList.Clear ();

						
								if (livingMahjongList.Count==0) {
									
									StartGameClear ();
								}
							
							
							
							}
					
					
					}



			
				} else {

					//shake并变红

					SoundMaker.Instance.createSoundCantChoose ();
					iOSHapticFeedback.Instance.Trigger ((iOSHapticFeedback.iOSFeedbackType)6);

					if (touchingCollider.gameObject.GetComponent<Mahjong> ().isShaking == false) {
						
						touchingCollider.gameObject.GetComponent<Mahjong> ().Shake ();

					}

					TurnRedBlockMahjongs (touchingCollider.gameObject);


				}

			}

		


	}








	}//	update


}
